export interface Game{
    background_image:string;
    name:string;
    released:string;
    metacritic_url:string;
    genres:Array<Genres>;
    parent_platforms:Array<ParentPlatforms>;
    publichers:Array<Publishers>;
    ratiing:Array<Rating>;
    screenshots:Array<Screenshots>;
    trailer:Array<Trailer>;

}

export interface APIResponse<T>{
    results:Array<T>


}



export interface Genres{
    
        name:string
    }


export interface ParentPlatforms{
    platform: {
        name:string
    };
}


export interface Publishers{
    name:string;
}


export interface Rating{
    id:number;
    count:number;
    title:string;
}


export interface Screenshots{
   image:string;
}


export interface Trailer{
   data:{
       max:string
   };
}
